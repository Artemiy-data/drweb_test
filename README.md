Сборка проекта:
```
python setup.py bdist_wheel
```

Запуск тестов:
```
pip install -r requirements/development.txt
pytest tests --cov package
```

Deploy проекта ansible:
```
ansible-playbook ansible/deploy.yml -i ansible/inventory.yml -vv --extra-vars "version=master"
```